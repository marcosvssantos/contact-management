<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    CONST TYPE_SUCCESS = 'success';
    CONST TYPE_ERROR = 'danger';
    CONST TYPE_WARNING = 'warning';
    public function index()
    {
        $contacts = Contact::all();
        return view('list', compact('contacts'));
    }

    public function create()
    {
        return view('new-contact');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5|max:255',
            'contact' => 'required|numeric|unique:contacts|digits:9',
            'email' => 'required|email|unique:contacts|max:255'
        ]);

        $contact = new Contact();

        $contact->name = $request->name;
        $contact->contact = $request->contact;
        $contact->email = $request->email;

        if (!$contact->save()) {
            return redirect(route('contact.list'))->with($this->notification(
                self::TYPE_ERROR,
                'Error saving contact'
            ));
        }

        return redirect(route('contact.list'))->with($this->notification(
            self::TYPE_SUCCESS,
            'Contact saved successfully'
        ));
    }

    public function show(Contact $contact)
    {
        return view('show', compact('contact'));
    }

    public function edit(Contact $contact)
    {
        return view('edit-contact', compact('contact'));
    }

    public function update(Request $request, Contact $contact)
    {
        $request->validate([
            'name' => 'required|min:5|max:255',
            'contact' => 'required|numeric|digits:9|unique:contacts,contact,' . $contact->id,
            'email' => 'required|email|max:255|unique:contacts,email,' . $contact->id
        ]);

        $contact->name = $request->name;
        $contact->contact = $request->contact;
        $contact->email = $request->email;

        if (!$contact->save()) {
            return redirect(route('contact.list'))->with($this->notification(
                self::TYPE_ERROR,
                'Error updating contact'
            ));
        }

        return redirect(route('contact.list'))->with($this->notification(
            self::TYPE_SUCCESS,
            'Contact updated successfully'
        ));
    }

    public function destroy(Contact $contact)
    {
        if (!$contact->delete()) {
            return redirect(route('contact.list'))
                ->with($this->notification(
                    self::TYPE_ERROR,
                    'Error deleting contact'
                ));
        }

        return redirect(route('contact.list'))
            ->with($this->notification(
                self::TYPE_SUCCESS,
                'Contact successfully deleted'
            ));
    }

    private function notification($type, $message)
    {
        return [
            'alert' => [
                'type' => $type,
                'message' => $message
            ]
        ];
    }
}
