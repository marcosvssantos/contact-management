<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'User test',
                'email' => 'user@test.com',
                'password' => Hash::make('secret'),
            ]
        ];
        DB::table('users')->insert($users);
    }
}
