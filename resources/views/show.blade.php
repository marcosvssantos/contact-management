@extends('layout.base')

@section('content')
    <div class="row">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h3>Contact</h3>
                        </div>
                        <div class="col-md-4 text-right">
                            <a class="btn btn-sm btn-outline-secondary" href="{{route('contact.list')}}" role="button">Back</a>
                            <form action="{{route('contact.delete', ['contact' => $contact->id])}}"
                                  method="post" style="display: inline">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <a href="#" class="btn btn-sm btn-outline-danger" data-action="delete">
                                   Delete
                                </a>
                            </form>
                            <a class="btn btn-sm btn-outline-primary" href="{{route('contact.edit', ['contact' => $contact->id])}}" role="button">Edit</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div>
                        <h6>ID</h6>
                        <p>{{$contact->id}}</p>
                    </div>
                    <div>
                        <h6>Name</h6>
                        <p>{{$contact->name}}</p>
                    </div>
                    <div>
                        <h6>Contact</h6>
                        <p>{{$contact->contact}}</p>
                    </div>
                    <div>
                        <h6>E-mail</h6>
                        <p>{{$contact->email}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function () {
            $('a[data-action="delete"]').on('click', function () {
                const $form = $(this).closest('form');
                swal({
                    title: "Attention",
                    text: "Are you sure you want to delete this contact?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $form.submit();
                        }
                    });
            })
        })
    </script>
@endpush
