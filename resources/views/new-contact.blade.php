@extends('layout.base')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2 class="mb-3">New contact</h2>
            <form action="{{route('contact.store')}}" method="post" class="needs-validation" novalidate>
                @csrf
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
                        <label for="name">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" aria-describedby="validationName" id="name" name="name" placeholder="" value="{{old('name')}}" required>
                        @error('name')
                            <div id="validationName" class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb-3">
                        <label for="contact">Contact</label>
                        <input type="text" class="form-control @error('contact') is-invalid @enderror" aria-describedby="validationContact" id="contact" name="contact" placeholder="" value="{{old('contact')}}" required>
                        @error('contact')
                        <div id="validationContact" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3">
                        <label for="email">E-mail</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" aria-describedby="validationEmail" id="email" name="email" placeholder="" value="{{old('email')}}" required>
                        @error('email')
                        <div id="validationEmail" class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3 text-right">
                        <a class="btn btn-outline-secondary btn-md" href="{{route('contact.list')}}" role="button">Cancel</a>
                        <button class="btn btn-primary btn-md" type="submit">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
