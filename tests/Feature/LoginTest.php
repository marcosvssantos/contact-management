<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{

    /**
     *
     * @return void
     */
    public function test_load_login_page()
    {
        $response = $this->get('/login');
        $response->assertSeeText('Login');
    }

    /**
     *
     * @return void
     */
    public function test_check_login()
    {
        $response = $this->post(route('login'), [
            'email' => 'user@test.com',
            'password' => 'secret'
        ]);
        $response->assertRedirect(route('contact.list'));
    }

    /**
     *
     * @return void
     */
    public function test_redirect_login_page_access_view()
    {
        $response = $this->get('/1');
        $response->assertRedirect(route('login'));
    }

    /**
     *
     * @return void
     */
    public function test_redirect_login_page_access_create()
    {
        $response = $this->get('/create');
        $response->assertRedirect(route('login'));
    }


    /**
     *
     * @return void
     */
    public function test_redirect_login_page_access_edit()
    {
        $response = $this->get('/1/edit');
        $response->assertRedirect(route('login'));
    }

    /**
     *
     * @return void
     */
    public function test_redirect_login_page_access_delete()
    {
        $response = $this->delete('/1');
        $response->assertRedirect(route('login'));
    }

}
