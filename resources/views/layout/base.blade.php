<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Contact Management</a>
        <ul class="navbar-nav px-3">
            @if(Auth::check())
            <li class="nav-item text-nowrap">
                <form id="flogout" action="{{route('logout')}}" method="post">
                    @csrf
                </form>
                <a id="logout" class="nav-link" href="#">Sign out</a>
            </li>
            @else
                <li class="nav-item text-nowrap">
                    <a class="nav-link" href="{{route('login')}}">Login</a>
                </li>
            @endif
        </ul>
</nav>

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('/') ? 'active' : ''}}" href="{{route('contact.list')}}">
                            <span data-feather="file-text"></span>
                            Contact list
                        </a>
                    </li>
                    @if(Auth::check())
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('create') ? 'active' : ''}}"
                               href="{{route('contact.create')}}">
                                <span data-feather="file-plus"></span>
                                New contact
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </nav>
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            @yield('content')
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript  -->
<script src="{{ asset('assets/js/jquery/jquery-slim.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>

<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@stack('script')

<script>
    $(function () {
        $('#logout').on('click', function (e) {
            $('#flogout').submit();
        })
    })
</script>

</body>
</html>

