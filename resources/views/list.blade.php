@extends('layout.base')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-8">
                    <h2>Contact list</h2>
                </div>

                @if(Auth::check())
                    <div class="col-md-4 text-right">
                        <a class="btn btn-outline-primary btn-md" href="{{route('contact.create')}}" role="button">New
                            contact</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @if(session('alert'))
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-{{session('alert')['type']}} alert-dismissible fade show" role="alert">
                    {{session('alert')['message']}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Contact</th>
                        <th>E-mail</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($contacts->isNotEmpty())
                        @foreach($contacts as $contact)
                            <tr>
                                <td>{{$contact->id}}</td>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->contact}}</td>
                                <td>{{$contact->email}}</td>
                                <td>
                                    @if(Auth::check())
                                        <a href="{{route('contact.show', ['contact' => $contact->id])}}"
                                           class="btn-xs btn-default btn-icon-action" data-bs-toggle="tooltip"
                                           data-bs-placement="top" title="View">
                                            <span data-feather="eye"></span>
                                        </a>

                                        <a href="{{route('contact.edit', ['contact' => $contact->id])}}"
                                           class="btn-xs btn-default btn-icon-action" data-bs-toggle="tooltip"
                                           data-bs-placement="top" title="Edit">
                                            <span data-feather="edit"></span>
                                        </a>

                                        <form action="{{route('contact.delete', ['contact' => $contact->id])}}"
                                              method="post" style="display: inline">
                                            @csrf
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a href="#" class="btn-xs btn-default btn-icon-action" data-action="delete"
                                               data-bs-toggle="tooltip" data-bs-placement="top" title="Delete">
                                                <span data-feather="trash-2"></span>
                                            </a>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">No contact registered</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(function () {
            $('table tbody tr td a[data-action="delete"]').on('click', function () {
                const $form = $(this).closest('form');
                swal({
                    title: "Attention",
                    text: "Are you sure you want to delete this contact?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $form.submit();
                        }
                    });
            })
        })
    </script>
@endpush
