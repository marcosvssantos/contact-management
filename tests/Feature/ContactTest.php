<?php

namespace Tests\Feature;

use App\models\Contact;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ContactTest extends TestCase
{
    use RefreshDatabase;

    public function test_check_store_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $response = $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHas('alert', [
            'type' => 'success',
            'message' => "Contact saved successfully"
        ]);
    }

    public function test_check_duplicate_store_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $response = $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('contact', 'email');
    }

    public function test_check_update_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $contact = Contact::where('email', "email@test.com")->first();

        $response = $this->actingAs($user)
            ->put(route('contact.update', ['contact' => $contact->id]), [
                'name' => 'Marcos Vinicius',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHas('alert', [
            'type' => 'success',
            'message' => "Contact updated successfully"
        ]);
    }

    public function test_check_error_name_store_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $response = $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Mar',
                'contact' => '123123123',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('name');
    }

    public function test_check_error_contact_store_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $response = $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '1231231233',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('contact');
    }

    public function test_check_error_email_store_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $response = $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123123',
                'email' => 'email',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('email');
    }

    public function test_check_error_name_update_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $contact = Contact::where('email', "email@test.com")->first();

        $response = $this->actingAs($user)
            ->put(route('contact.update', ['contact' => $contact->id]), [
                'name' => 'Marc',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('name');
    }

    public function test_check_error_contact_update_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $contact = Contact::where('email', "email@test.com")->first();

        $response = $this->actingAs($user)
            ->put(route('contact.update', ['contact' => $contact->id]), [
                'name' => 'Marcos Vinicius',
                'contact' => '12312312443',
                'email' => 'email@test.com',
            ]);

        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('contact');
    }

    public function test_check_error_email_update_contact()
    {
        $this->seed();
        $user = User::where('email', "user@test.com")->first();

        $this->actingAs($user)
            ->post(route('contact.store'), [
                'name' => 'Marcos',
                'contact' => '123123124',
                'email' => 'email@test.com',
            ]);

        $contact = Contact::where('email', "email@test.com")->first();

        $response = $this->actingAs($user)
            ->put(route('contact.update', ['contact' => $contact->id]), [
                'name' => 'Marcos Vinicius',
                'contact' => '123123124',
                'email' => 'email',
            ]);
        $response->assertRedirect(route('contact.list'));
        $response->assertSessionHasErrors('email');
    }
}
